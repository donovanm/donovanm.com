var myApp = angular.module('myApp', ['ngRoute', 'ngAnimate']);

myApp.config(function ($routeProvider, $locationProvider) {
	$routeProvider
		.when('/',
			{
				controller  : 'MainCtrl',
				templateUrl : 'partials/main.html'
			})
		.when('/skills',
			{
				controller  : 'SkillCtrl',
				templateUrl : 'partials/skills.html'
			})
		.when('/portfolio',
			{
				controller  : 'PortfolioCtrl',
				templateUrl : 'partials/portfolio.html'
			})
		.when('/about',
			{
				controller  : 'AboutCtrl',
				templateUrl : 'partials/about.html'
			})
		.when('/portfolio/turkmaster',
			{
				controller  : 'TurkmasterCtrl',
				templateUrl : 'partials/turkmaster.html'
			})
		.otherwise({ redirectTo: '/' });

		$locationProvider
			.html5Mode(false)
			.hashPrefix('!');
});

myApp.controller('TurkmasterCtrl', function($scope) {
	$("header").addClass("narrow");
	$("body").addClass("dark");

	setTimeout(function() {
		$("#video-container").append('<iframe width="560" height="349" src="//www.youtube-nocookie.com/embed/O-HxZjfDlAI?autoplay=1" frameborder="0" allowfullscreen></iframe>');
	}, 500);

	$scope.title = "Turkmaster";
});

myApp.controller('MainCtrl', function($scope) {
	$("body").removeClass("dark");
	$("header").removeClass("narrow");

	$scope.links = [
		{
			title    : "Portfolio",
			subtitle : "My most recent work",
			href     : "portfolio"
		},
		{
			title    : "Skills",
			subtitle : "My technical knowledge and experience",
			href     : "skills"
		},
		{
			title    : "About Me",
			subtitle : "Learn a little about who I am",
			href     : "about"
		}
	]
});

myApp.controller('SkillCtrl', function($scope) {
	$("body").removeClass("dark");
	$("header").addClass("narrow");

	$scope.title = "Skills";
	$scope.skillset = [
		{
			type: "Languages",
			skills: [
				{ skill : "Javascript", proficiency: 3 },
				{ skill : "Java" , proficiency: 2 },
				{ skill : "PHP"  , proficiency: 2 },
				{ skill : "C++"  , proficiency: 1 }
			]
		},
		{
			type: "Libraries",
			skills: [
				{ skill : "jQuery"    , proficiency: 3 },
				{ skill : "AngularJS" , proficiency: 2 },
				{ skill : "Bootstrap" , proficiency: 1 },
				{ skill : "Android Framework", proficiency: 1 }
			]
		},
		{
			type: "Database",
			skills: [
				{ skill : "MySQL"     , proficiency: 2 },
				{ skill : "IndexedDB" , proficiency: 1}
			]
		},
		{
			type: "Other",
			skills: [
				{ skill : "HTML5" , proficiency: 3 },
				{ skill : "CSS3"  , proficiency: 3 },
				{ skill : "AJAX"  , proficiency: 3 },
				{ skill : "JSON"  , proficiency: 3 }
			]
		},
		{
			type: "Tools",
			skills: [
				{ skill : "Sublime Text", proficiency: 3 },
				{ skill : "git"         , proficiency: 2 },
				{ skill : "Bitbucket"   , proficiency: 2 },
				{ skill : "Sass"        , proficiency: 1 }
			]
		}
	];

	// Determine classes here instead of bloated template
	var skills = [ "novice", "intermediate", "proficient", "expert"];
	 
	for (var i = 0; i < $scope.skillset.length; i++) {
		var group = $scope.skillset[i];
		for (var j = 0, len = group.skills.length; j < len; j++) {
			var skill = group.skills[j],
				prof = skill.proficiency;

			skill.class = skills[prof - 1];
		}
	}

	// Highlight skills on hover
	$("#key").find("li").hover(highlight, unhighlight);

	function highlight(e) {
		var myClass,
			types = ['expert', 'proficient', 'intermediate', 'novice'];

		for (var i = 0; i < types.length; i++)
			if ($(e.target).hasClass(types[i]))
				myClass = types[i];

		$("#skillset").find("li." + myClass).addClass("highlight");
	}

	function unhighlight(e) {
		$("#skillset").find("li").removeClass("highlight");
	}

});

myApp.controller('PortfolioCtrl', function($scope) {
	$("body").removeClass("dark");
	$("header").addClass("narrow");

	$scope.title = "Portfolio";
	$scope.projects = [
		{
			title: "Custom newsletter system",
			description: "An internal contact management and email system created for a small business to send news blasts to its customers. Feel free to add/edit/delete anything. It's just a demo.",
			thumb: "mail.png",
			technologies: [ "AngularJS", "jQuery", "AJAX", "PHP", "MySQL", "RESTful API", "CSS3" ],
			links: [
				{
					title: "Demo",
					url: "/newsblasts/"
				}
			]
		},
		{
			title: "Turkmaster",
			description: "A page scraping and monitoring utility designed for Amazon's Mechanical Turk crowdsource website. Released recently, there are already 150+ workers using the utlity to increase their productivity.",
			thumb: "turkmaster.png",
			technologies: [ "jQuery", "CSS3", "AJAX", "JSON", "HTML5 Audio" ],
			links: [
				{
					title: "Video",
					url: "#!/portfolio/turkmaster"
				},
				{
					title: "Download",
					url: "https://greasyfork.org/scripts/4771-turkmaster",
					external: true
				}
			]
		},
		{
			title: "Music artist website",
			description: "A fairly straightforward artist page built with AngularJS for it's single page app capability. This interactive website includes a fluidly-animated photo gallery and a custom-built HTML5 music player. Warning: Music auto-plays and may have explicit lyrics.",
			thumb: "artist.png",
			technologies: [ "AngularJS", "jQuery", "CSS3", "HTML5 Audio" ],
			links: [
				{
					title: "Website",
					url: "http://kidliononline.com/",
					external: true
				}
			]
		}
	]
});

myApp.controller('AboutCtrl', function($scope) {
	$("body").removeClass("dark");
	$("header").addClass("narrow");

	$scope.title = "About Me";
	$scope.details = [
		"I'm a software developer who, after college, kept finding myself working in fields unrelated to what I love. Every so often I would get the urge to program something and would take on little projects only to get drawn away by Real Life(tm). One day I finally decided that I should do what I actually love to do for a living so here I am.",
		"I have a background in traditional programming languages such as C++ and Java and have done previous work with HTML, CSS and PHP. In the past year or so, however, I have gradually discovered just how far along Javascript has come since I last used with it (or maybe how much I underestimated it). Since then I have been learning and practicing new techniques and patterns associated with 'modern' Javascript coding in my newer projects and in my free time. I have to admit I was wrong about Javascript!",
		"If you have any inquiries feel free to contact me at my 'donovan.munerlyn' gmail address (please insert the '@'' and '.com'. I'm trying to avoid spam.)"
	];
});
