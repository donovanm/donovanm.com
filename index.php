<?php

if (isset($_GET) && array_key_exists('_escaped_fragment_', $_GET))
	$request = $_GET['_escaped_fragment_'];

?><!DOCTYPE html>
<html data-ng-app="myApp">
<head>
	<meta charset="utf-8">
	<title>DonovanM.com<?php if (isset($request)) echo ' - ' . $request ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="author" content="Donovan Munerlyn">
	<meta name="web_author" content="Donovan Munerlyn">
	<meta name="fragment" content="!">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular.min.js"></script>
</head>
<body class="container-fluid">
	<header class="row">
		<h1 class="col-md-8"><a href="#!">Donovan Munerlyn<br /><small>Software Developer</small></a></h1>
		<nav class="col-md-4 clearfix">
			<ul class="clearfix">
				<li><a href="#!portfolio">Portfolio</a></li>
				<li><a href="#!skills">Skills</a></li>
				<li><a href="#!about">About</a></li>
			</ul>
		</nav>
	</header>
	<main data-ng-view="" class="row anim">
<?php
	if (isset($request)) {
		$page = str_replace('/', '-', $request);

		if ($page == '')
			$page = 'main';

		$url = 'snapshots/' . $page . '.html';
		include($url);
	}
?>
	</main>
	<footer class="row">
		&copy;2014 Donovan Munerlyn. All rights reserved.
	</footer>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.24/angular-animate.js"></script>
	<script src="js/angular-route.min.js"></script>
	<script src="js/app.js"></script>
</body>
</html>